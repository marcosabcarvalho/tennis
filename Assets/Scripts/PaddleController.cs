﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {

	public float speed = 5;

	public float direction;

	public Transform upperLimit;
	public Transform lowerLimit;

	public bool isPlayerOne;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isPlayerOne){
			//Movement paddle1 Y
			if(Input.GetKey(KeyCode.W)){
				//Movement Code
				transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
				direction = 1;
			} else if(Input.GetKey(KeyCode.S)){
				//Movement Code
				transform.position = new Vector3(transform.position.x, transform.position.y - (speed * Time.deltaTime), transform.position.z);
				direction = -1;
			} else {
				direction = 0;
			}
				//Movement paddle1 X
				if(Input.GetKey(KeyCode.E)){
				//Movement Code
				transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
				direction = 1;
			} else if(Input.GetKey(KeyCode.Q)){
				//Movement Code
				transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), transform.position.y, transform.position.z);
				direction = -1;
			} else {
				direction = 0;
			}
		} else {
			//Movement paddle2 Y
			if(Input.GetKey(KeyCode.O)){
				//Movement Code
				transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
				direction = 1;
			} else if(Input.GetKey(KeyCode.L)){
				//Movement Code
				transform.position = new Vector3(transform.position.x, transform.position.y - (speed * Time.deltaTime), transform.position.z);
				direction = -1;
			} else {
				direction = 0;
			}
			//Movement paddle2 X
			if(Input.GetKey(KeyCode.P)){
				//Movement Code
				transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
				direction = 1;
			} else if(Input.GetKey(KeyCode.I)){
				//Movement Code
				transform.position = new Vector3(transform.position.x - (speed * Time.deltaTime), transform.position.y, transform.position.z);
				direction = -1;
			} else {
				direction = 0;
			}
		}
		//Limit upper lower paddle
		if (transform.position.y > upperLimit.position.y)
		{
			transform.position = new Vector3(transform.position.x, upperLimit.position.y, transform.position.z);
		} else if (transform.position.y < lowerLimit.position.y)
			{
				transform.position = new Vector3(transform.position.x, lowerLimit.position.y, transform.position.z);
			}

	}

	void OnCollisionEnter2D(Collision2D other) {
		other.rigidbody.velocity = new Vector2(other.rigidbody.velocity.x * 1.2f, other.rigidbody.velocity.y + direction);
		//Debug.Log(other.rigidbody.velocity);
	}
}
