﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

	public float startForce = 5;

	private Rigidbody2D myRigiBody;

	public GameObject paddle1;

	public GameObject paddle2;

	public GameManager theGM;

	// Use this for initialization
	void Start () {
		myRigiBody = GetComponent<Rigidbody2D>();
		myRigiBody.velocity = new Vector2(1.8f * startForce, startForce);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	//If across goal, increase score
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "GoalZone") 
		{
			if (transform.position.x < 0) {
				transform.position = paddle2.transform.position + new Vector3(-1f,0,0);
				myRigiBody.velocity = new Vector2(-1.8f * startForce, -startForce);
				theGM.UpdateScore(2);
			} else {
				transform.position = paddle1.transform.position + new Vector3(1f,0,0);
				myRigiBody.velocity = new Vector2(1.8f * startForce, startForce);
				theGM.UpdateScore(1);
			}
		}

	}
}
