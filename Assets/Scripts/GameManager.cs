﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Text scoreText;

	private int score1;
	private int score2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void UpdateScore(int player) {
		if (player == 1 )
		{
			score1 += 1;
		}
		if (player == 2 )
		{
			score2 += 1;
		}
		scoreText.text = score1 + " - " + score2;
	}
}
